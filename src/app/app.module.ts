import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { MDBBootstrapModule } from 'angular-bootstrap-md';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NavbarComponent } from './share/component/navbar/navbar.component';
import { FooterComponent } from './share/component/footer/footer.component';
import { PageNotFoundComponent } from './share/component/page-not-found/page-not-found.component';
import { LoginComponent } from './share/component/login/login.component';
import { ForbiddenComponent } from './share/component/forbidden/forbidden.component';
import { InternalServerErrorComponent } from './share/component/internal-server-error/internal-server-error.component';
import { LayoutComponent } from './share/layout/layout.component';

@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    FooterComponent,
    PageNotFoundComponent,
    LoginComponent,
    ForbiddenComponent,
    InternalServerErrorComponent,
    LayoutComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    MDBBootstrapModule.forRoot(),
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
