import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { NavbarComponent } from './share/component/navbar/navbar.component';
import { FooterComponent } from './share/component/footer/footer.component';
import { PageNotFoundComponent } from './share/component/page-not-found/page-not-found.component';
import { LoginComponent } from './share/component/login/login.component';
import { ForbiddenComponent } from './share/component/forbidden/forbidden.component';
import { InternalServerErrorComponent } from './share/component/internal-server-error/internal-server-error.component';
import { LayoutComponent } from './share/layout/layout.component';
const routes: Routes = [
  {
    path: 'dashboard', component: LayoutComponent, children: [


    ]
  },
  {
    path: 'login', component: LoginComponent
  },
  {
    path: '', redirectTo: 'dashboard', pathMatch: 'full'
  },
  {
    path: '404', component: PageNotFoundComponent
  },
  {
    path: '403', component: ForbiddenComponent
  },
  {
    path: '500', component: InternalServerErrorComponent
  },
  {
    path: '**', component: PageNotFoundComponent
  }

];


@NgModule({
  imports: [RouterModule.forRoot(routes, { scrollPositionRestoration: 'enabled' }),],
  exports: [RouterModule]
})

export class AppRoutingModule { }
